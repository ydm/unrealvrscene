// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include <cstdio>
#include <functional>


class VRSValue
{
public:
     using GenericListType = TArray<VRSValue *>;
     using IntListType = TArray<int32>;
     using StringListType = TArray<FString>;
     using VectorListType = TArray<FVector>;

     static VRSValue *NewColor(float, float, float, float);
     static VRSValue *NewFloat(float);
     static VRSValue *NewInt(int);
     static VRSValue *NewMatrix(const FVector&, const FVector&, const FVector&);
     static VRSValue *NewReference(const char *);
     static VRSValue *NewString(const char *);
     static VRSValue *NewTransform(const FMatrix&, const FVector&);
     static VRSValue *NewVector(float, float, float);
     static VRSValue *NewGenericList(GenericListType *List = nullptr);
     static VRSValue *NewIntList(IntListType *List = nullptr);
     static VRSValue *NewStringList(StringListType *List = nullptr);
     static VRSValue *NewVectorList(VectorListType *List = nullptr);

     inline const FColor& GetColor() const { return ColorValue; }
     inline float GetFloat() const { return FloatValue; }
     inline float GetInt() const { return U.IntValue; }
     inline const FMatrix& GetMatrix() const { return MatrixValue; }
     inline const FString& GetReference() const { return StrValue; }
     inline const FString& GetString() const { return StrValue; }
     inline const FMatrix& GetTransform() const { return MatrixValue; }
     inline const FVector& GetVector() const { return VectorValue; }
     inline GenericListType *GetGenericList() { return U.GenericListValue; }
     inline IntListType *GetIntList() { return U.IntListValue; }
     inline StringListType *GetStringList() { return U.StringListValue; }
     inline VectorListType *GetVectorList() { return U.VectorListValue; }

     ~VRSValue();
     void Print() const;

     enum Type {
          T_Float,
          T_Integer,
          T_String,
          T_Vector,
          T_Matrix,
          T_Transform,
          T_GenericList,
          T_IntList,
          T_StringList,
          T_VectorList,
          T_Color,
          T_Reference,
     } NodeType;

     FColor                  ColorValue;
     float                   FloatValue;
     FMatrix                 MatrixValue;
     FString                 StrValue;
     FVector                 VectorValue;
     union {
          int                 IntValue;
          GenericListType    *GenericListValue;
          IntListType        *IntListValue;
          StringListType     *StringListValue;
          VectorListType     *VectorListValue;
     } U;

private:
     VRSValue();
     VRSValue& operator=(const VRSValue& other);
     VRSValue& operator=(VRSValue&& other);

     template<typename V>
     void PrintArray(const TArray<V>& Ary, std::function<void(const V&)> PrintFn) const
          {
               std::printf("List(");
               bool First = true;
               for (const auto& Value : Ary)
               {
                    if (First == false)
                    {
                         std::printf(", ");
                    }
                    PrintFn(Value);
                    First = false;
               }
               std::printf(")");
          }
};


class VRSceneParser;


class VRSObject
{
public:
     using PairMap = TMap<FString, VRSValue *>;

     VRSObject(const FString& ClassName, const FString& InstanceName, PairMap& Pairs);
     ~VRSObject();
     /**
      * Follow a reference and return the VRSObject
      * behind it.  If nothing found, return nullptr.
      */
     VRSObject *Follow(VRSceneParser& Owner, const FString& Key);
     inline const FString& GetClassName() const { return ClassName; }
     inline const FString& GetInstanceName() const { return InstanceName; }
     void Print() const;
     inline VRSValue *operator[](const FString& Key) { return Pairs[Key]; }
     

     

private:
     const FString ClassName;
     const FString InstanceName;
     PairMap Pairs;
};


/**
 *
 */
class VRSceneParser
{
public:
    using ObjectMap = TMap<FString, VRSObject *>;

    static VRSceneParser *GetActiveInstance();

    VRSceneParser();
    ~VRSceneParser();
    VRSObject *Follow(VRSObject& Object, const FString& Key);
    inline ObjectMap& GetObjects() { return Objects; }
    bool Parse(const FString& Filename);
    void Print() const;
    inline VRSObject *operator[](const FString& Key) const { return Objects[Key]; }

private:
    static void SetActiveInstance(VRSceneParser *Parser);

    void FreeObjects();
    void NewObject(const char *ClassName, const char *InstanceName);
    void NewPair(const char *Key, VRSValue *Value);

    VRSObject::PairMap Pairs;
    ObjectMap Objects;

    friend int yyparse();
};
