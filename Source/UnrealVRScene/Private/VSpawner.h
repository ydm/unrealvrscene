// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "VRSceneParser.h"
#include "VSpawner.generated.h"


class VRSObject;

UCLASS()
class AVSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVSpawner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = VRScene)
    FString VRSceneFilename;
    
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = VRScene)
	float ScaleFactor;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = VRScene)
    FRotator BaseRotation;

private:
    void InflateScene();
    void SpawnNode(VRSObject& Node);

    VRSceneParser Parser;
};
