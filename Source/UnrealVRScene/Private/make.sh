#!/bin/bash

# Clean up
rm -f grammar.tab.c grammar.tab.h lex.yy.cc
rm -f grammar.tab.cc grammar.tab.hh lex.yy.cc
rm -f grammar.tab.cpp grammar.tab.hpp lex.yy.cpp
rm -f grammar.tab.inc grammar.tab.hpp lex.yy.inc

# Generate
flex                          -o lex.yy.inc      patterns.l 
bison --defines=grammar.tab.h -o grammar.tab.inc grammar.y  
