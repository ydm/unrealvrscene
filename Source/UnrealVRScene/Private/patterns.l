%option noyywrap

%{
  #include <cstdio>
  #include <cstdlib>
  #include "grammar.tab.h"

  void  yyerror (const char *);

  static char *copy_string (const char *const s, const int len)
  {
    const size_t u = static_cast<size_t> (len);
    char *const copy = new char[u + 1];
    strncpy (copy, s, u);
    copy[u] = '\0';
    return copy;
  }
%}

ID [a-zA-Z_][a-zA-Z0-9:@_|]*
DSTAR [0-9]*
DPLUS [0-9]+
E [Ee][+\-][0-9]+


%%

"AColor"     { return TOK_ACOLOR;      }
"Color"      { return TOK_COLOR;       }
"List"       { return TOK_LIST;        }
"ListInt"    { return TOK_LIST_INT;    }
"ListString" { return TOK_LIST_STRING; }
"ListVector" { return TOK_LIST_VECTOR; }
"Matrix"     { return TOK_MATRIX;      }
"Transform"  { return TOK_TRANSFORM;   }
"Vector"     { return TOK_VECTOR;      }


-?{DPLUS}(\.{DSTAR}{E}?f?|{E}f?) {
  yylval.f = std::strtof (yyget_text (), nullptr);
  return TOK_FLOAT;
}

-?{DSTAR}\.{DPLUS}{E}?f? {
  yylval.f = std::strtof (yyget_text (), nullptr);
  return TOK_FLOAT;
}

{ID} {
  yylval.s = copy_string (yyget_text (), yyget_leng ());
  return TOK_IDENTIFIER;
}

-?[0-9]+ {
  yylval.i = atoi(yyget_text ());
  return TOK_INTEGER;
}

\"(\\.|[^\"])*\" {
  /* String */
  yylval.s = copy_string (yyget_text (), yyget_leng ());
  return TOK_STRING;
}

[(),;={}] {
  /* Char literals */
  return *yyget_text ();
}

[ \t] {
  /* Skip whitespace */
}

"//".* {
  /* Skip comments */
}

. {
  // char s[128];
  // sprintf (s, "invalid character: %c", *yyget_text ());
  // yyerror (s);
}

%%
