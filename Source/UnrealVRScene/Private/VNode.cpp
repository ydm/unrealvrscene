// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealVRScene.h"
#include "VNode.h"
#include "VRSceneParser.h"
#include "VStaticMeshComponent.h"


// Sets default values
AVNode::AVNode()
{
     // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
     PrimaryActorTick.bCanEverTick = false;
     StaticMesh = CreateDefaultSubobject<UVStaticMeshComponent>(TEXT("Procedural Mesh"));
     RootComponent = StaticMesh;
}

// Called when the game starts or when spawned
void AVNode::BeginPlay()
{
     Super::BeginPlay();
}

// Called every frame
void AVNode::Tick( float DeltaTime )
{
     Super::Tick( DeltaTime );
}

void AVNode::LoadNodeFromVRSObject(VRSceneParser& Parser, VRSObject& Node, const float Scale)
{
     // TODO: Make a routine that checks if an object is a valid
     // Node (and has all the required keys set to valid objects).
    
     const FMatrix& Root = Node[TEXT("transform")]->GetTransform();
     VRSObject *const Geometry = Node.Follow(Parser, TEXT("geometry"));
     LoadStaticMesh(Root, *Geometry, Scale);
}

void AVNode::LoadStaticMesh(const FMatrix& Root, VRSObject& Geometry, const float Scale)
{
    // Assert it's a valid Geometry object
    VRSValue::VectorListType& Vertices = *Geometry["vertices"]->GetVectorList();
    for (auto& Vector : Vertices)
    {
        Vector = Root.TransformVector(Vector);
        Vector += Root.GetOrigin();
        Vector *= Scale;
    }
    VRSValue::VectorListType& Normals = *Geometry["normals"]->GetVectorList();
    for (auto& Vector : Normals)
    {
        Vector = Root.TransformVector(Vector);
    }

    VRSValue::IntListType& Triangles = *Geometry["faces"]->GetIntList();

    const TArray<FVector2D> UV0;
    const TArray<FColor> VertexColors;
    const TArray<FProcMeshTangent> Tangents;

    StaticMesh->CreateMeshSection(0, Vertices, Triangles, Normals, UV0, VertexColors, Tangents, true);
}
