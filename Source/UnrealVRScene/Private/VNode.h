// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "VNode.generated.h"


class VRSceneParser;


class VRSObject;

UCLASS()
class AVNode : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AVNode();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void LoadNodeFromVRSObject(VRSceneParser& Parser, VRSObject& Node, float Scale);

private:
    void LoadStaticMesh(const FMatrix& Root, VRSObject& Geometry, float Scale);

    class UVStaticMeshComponent *StaticMesh;
};
