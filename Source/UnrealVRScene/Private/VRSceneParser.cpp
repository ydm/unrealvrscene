// Fill out your copyright notice in the Description page of Project Settings.

// Unreal includes
#include "UnrealVRScene.h"
#include "VRSceneParser.h"

// GNU Bison parser files generated from patterns.l and grammar.y
#include "lex.yy.inc"
#include "grammar.tab.inc"


namespace
{
    VRSceneParser *ActiveInstance = nullptr;

    inline FString AnsiToFString(const char *const Str)
    {
		return FString(ANSI_TO_TCHAR(Str));
    }

	inline uint8 ColorByte(const float x)
	{
		return static_cast<uint8>(x * 255.0f);
	}
}


// ------------------------
// VRSValue
// ------------------------

VRSValue *VRSValue::NewColor(const float R, const float G, const float B, const float A)
{
    VRSValue *const Node = new VRSValue();
    Node->NodeType = T_Color;
    Node->ColorValue = FColor(ColorByte(R), ColorByte(G), ColorByte(B), ColorByte(A));
    return Node;
}

VRSValue *VRSValue::NewFloat(const float Value)
{
    VRSValue *const Node = new VRSValue();
    Node->NodeType = T_Float;
    Node->FloatValue = Value;
    return Node;
}

VRSValue *VRSValue::NewInt(const int Value)
{
	VRSValue *const Node = new VRSValue();
	Node->NodeType = T_Integer;
	Node->U.IntValue = Value;
	// We also store the integer value as float
	Node->FloatValue = static_cast<float>(Value);
	return Node;
}

VRSValue *VRSValue::NewMatrix(const FVector& X, const FVector& Y, const FVector& Z)
{
    VRSValue *const Node = new VRSValue();
	Node->NodeType = T_Matrix;
	Node->MatrixValue = FMatrix(X, Y, Z, FVector::ZeroVector);
    return Node;
}

VRSValue *VRSValue::NewReference(const char *S)
{
    VRSValue *const Node = new VRSValue();
    Node->NodeType = T_Reference;
	Node->StrValue = AnsiToFString(S);
    return Node;
}

VRSValue *VRSValue::NewString(const char *S)
{
    VRSValue *const Node = new VRSValue();
	Node->NodeType = T_String;
	Node->StrValue = AnsiToFString(S);
    return Node;
}

VRSValue *VRSValue::NewTransform(const FMatrix& XYZ, const FVector& W)
{
	VRSValue *const Node = new VRSValue();
	Node->NodeType = T_Transform;
	Node->MatrixValue = XYZ;
	Node->MatrixValue.SetOrigin(W);
	Node->MatrixValue.M[3][3] = 1.0f;
	return Node;
}

VRSValue *VRSValue::NewVector(const float X, const float Y, const float Z)
{
    VRSValue *const Node = new VRSValue();
	Node->NodeType = T_Vector;
	Node->VectorValue.X = X;
	Node->VectorValue.Y = Y;
	Node->VectorValue.Z = Z;
    return Node;
}

VRSValue *VRSValue::NewGenericList(GenericListType *const List)
{
    VRSValue *const Node = new VRSValue();
	Node->NodeType = T_GenericList;
	Node->U.GenericListValue = List ? List : new GenericListType();
    return Node;
}

VRSValue *VRSValue::NewIntList(IntListType *const List)
{
    VRSValue *const Node = new VRSValue();
	Node->NodeType = T_IntList;
	Node->U.IntListValue = List ? List : new IntListType();
    return Node;
}

VRSValue *VRSValue::NewStringList(StringListType *const List)
{
    VRSValue *const Node = new VRSValue();
	Node->NodeType = T_StringList;
	Node->U.StringListValue = List ? List : new StringListType();
    return Node;
}

VRSValue *VRSValue::NewVectorList(VectorListType *const List)
{
    VRSValue *const Node = new VRSValue();
	Node->NodeType = T_VectorList;
	Node->U.VectorListValue = List ? List : new VectorListType();
    return Node;
}

VRSValue::VRSValue()
: NodeType(T_Float)
, ColorValue()
, FloatValue(0.0f)
, MatrixValue()
, StrValue()
, VectorValue()
{
    std::memset (&U, 0, sizeof(U));
}

VRSValue::~VRSValue()
{
    switch (NodeType)
    {
        case T_GenericList: {
            for (auto Value : *U.GenericListValue)
            {
                delete Value;
            }
            delete U.GenericListValue; break;
        }
        case T_IntList:    delete U.IntListValue;    break;
        case T_StringList: delete U.StringListValue; break;
        case T_VectorList: delete U.VectorListValue; break;
        default: break;
    }
}

VRSValue& VRSValue::operator=(const VRSValue& other) { return *this; }
VRSValue& VRSValue::operator=(VRSValue&& other) { return *this; }

void VRSValue::Print() const
{
    switch (NodeType)
    {
	case T_Color: {
		const FColor& C = ColorValue;
		std::printf("Color(%u, %u, %u, %u)", C.R, C.G, C.B, C.A);
	} break;
    case T_Float:     std::printf("%f", FloatValue); break;
    case T_Integer:   std::printf("%d", U.IntValue); break;
	case T_Matrix: {
		const FMatrix& M = MatrixValue;
		std::printf("[M [%f, %f, %f] [%f %f %f] [%f %f %f]]",
			M.M[0][0], M.M[0][1], M.M[0][2],
			M.M[1][0], M.M[1][1], M.M[1][2],
			M.M[2][0], M.M[2][1], M.M[2][2]);
	} break;
	case T_Reference: std::printf("%s", TCHAR_TO_ANSI(*StrValue)); break;
    case T_String:    std::printf("%s", TCHAR_TO_ANSI(*StrValue)); break;
	case T_Transform: {
		const FMatrix& M = MatrixValue;
		std::printf("[T [%f %f %f] [%f %f %f] [%f %f %f] [%f %f %f]]",
			M.M[0][0], M.M[0][1], M.M[0][2],
			M.M[1][0], M.M[1][1], M.M[1][2],
			M.M[2][0], M.M[2][1], M.M[2][2],
			M.M[3][0], M.M[3][1], M.M[3][2]);
	} break;
	case T_Vector: {
		const FVector& V = VectorValue;
		std::printf("[V %f %f %f]", V.X, V.Y, V.Z);
	} break;
	case T_GenericList: {
		PrintArray<VRSValue *>(*U.GenericListValue, [](const VRSValue *const & V) {
            V->Print();
        });
	} break;
    case T_IntList: {
        PrintArray<int32>(*U.IntListValue, [](const int32& V) {
            std::printf("%d", V);
        });
    }
    case T_StringList: {
        PrintArray<FString>(*U.StringListValue, [](const FString& V) {
            std::printf("%s", TCHAR_TO_ANSI(*V));
        });
    }
    case T_VectorList: {
        PrintArray<FVector>(*U.VectorListValue, [](const FVector& V) {
            std::printf("[%f, %f, %f]", V.X, V.Y, V.Z);
        });
    }
    }
}


// ------------------------
// VRSObject
// ------------------------

VRSObject::VRSObject(const FString& _ClassName, const FString& _InstanceName, PairMap& _Pairs)
: ClassName(_ClassName)
, InstanceName(_InstanceName)
{
    Pairs = MoveTemp(_Pairs);
    check(_Pairs.Num() == 0);
}

VRSObject::~VRSObject()
{
    for (auto& Pair : Pairs)
    {
        delete Pair.Value;
        Pair.Value = nullptr;
    }
}

VRSObject *VRSObject::Follow(VRSceneParser& Owner, const FString& Key)
{
    return Owner.Follow(*this, Key);
}

void VRSObject::Print() const
{
    printf("%s: %s\n", TCHAR_TO_ANSI(*InstanceName), TCHAR_TO_ANSI(*ClassName));
    for (const auto& Pair : Pairs)
    {
        printf("\t%s = ", TCHAR_TO_ANSI(*Pair.Key));
        Pair.Value->Print();
        printf("\n");
    }
}


// ------------------------
// VRSceneParser
// ------------------------

VRSceneParser *VRSceneParser::GetActiveInstance()
{
    return ActiveInstance;
}

void VRSceneParser::SetActiveInstance(VRSceneParser *const Parser)
{
    ActiveInstance = Parser;
}

VRSceneParser::VRSceneParser()
{
}

VRSceneParser::~VRSceneParser()
{
    FreeObjects();
}

VRSObject *VRSceneParser::Follow(VRSObject& Object, const FString& Key)
{
    const VRSValue *const Value = Object[Key];
    if (Value && Value->NodeType == VRSValue::T_Reference)
    {
        return Objects[Value->GetReference()];
    }
    return nullptr;
}

void VRSceneParser::FreeObjects()
{
    check(Pairs.Num() == 0);
    for (auto& Obj : Objects)
    {
        delete Obj.Value;
        Obj.Value = nullptr;
    }
    Objects.Reset();
    check(Objects.Num() == 0);
}

void VRSceneParser::NewObject(const char *const ClassName, const char *const InstanceName)
{
    const FString Key = AnsiToFString(InstanceName);
    Objects.Add(Key, new VRSObject(AnsiToFString(ClassName), Key, Pairs));
    check(Pairs.Num() == 0 && "Expected empty Pairs");
}

void VRSceneParser::NewPair(const char *const Key, VRSValue *const Value)
{
    const FString& FKey = AnsiToFString(Key);
    Pairs.Add(FKey, Value);
}

bool VRSceneParser::Parse(const FString& Filename)
{
    FreeObjects();
	std::FILE *File = std::fopen(TCHAR_TO_ANSI(*Filename), "r"); 
	if (File)
	{
        // TODO: Lock here
		yyset_in(File);
        SetActiveInstance(this);
		const int Result = yyparse();
        SetActiveInstance(nullptr);
		yylex_destroy();
        // TODO: Unlock here
		std::fclose(File);
		return Result == 0;
	}
	else
	{
		std::perror("fopen");
		return false;
	}
}

void VRSceneParser::Print() const
{
    for (const auto& Obj : Objects)
    {
        Obj.Value->Print();
        printf("\n");
    }
}
