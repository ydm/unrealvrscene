// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealVRScene.h"
#include "VCharacter.h"


// Sets default values
AVCharacter::AVCharacter()
: Super()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AVCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AVCharacter::Tick(float DeltaTime)
{
	Super::Tick( DeltaTime );
}

// Called to bind functionality to input
void AVCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// set up gameplay key bindings
	check(PlayerInputComponent);

    PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AVCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AVCharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AVCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AVCharacter::MoveRight);
    PlayerInputComponent->BindAxis("MoveUp", this, &AVCharacter::MoveUp);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}

void AVCharacter::MoveForward(const float Val)
{
    if (Val != 0.0f)
    {
        AddMovementInput(GetActorForwardVector(), Val);
    }
}

void AVCharacter::MoveRight(const float Val)
{
    if (Val != 0.0f)
    {
        AddMovementInput(GetActorRightVector(), Val);
    }
}

void AVCharacter::MoveUp(const float Val)
{
    if (Val != 0.0f)
    {
        AddMovementInput(GetActorUpVector(), Val);
    }
}
