#!/bin/bash

# Clean up
rm -f grammar.tab.c grammar.tab.h lex.yy.cc
rm -f grammar.tab.cc grammar.tab.hh lex.yy.cc
rm -f grammar.tab.cpp grammar.tab.hpp lex.yy.cpp
rm -f grammar.tab.inc grammar.tab.hpp lex.yy.inc

# Generate
FLEX=../../../Tools/Windows/Bison/win_flex
BISON=../../../Tools/Windows/Bison/win_bison

$FLEX  --wincompat             -o lex.yy.inc      patterns.l 
$BISON --defines=grammar.tab.h -o grammar.tab.inc grammar.y  
