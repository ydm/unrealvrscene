%{
  /* We're hosted by Unreal Engine */
  #include "EngineMinimal.h"

  /* Declarations */
  int   yyget_lineno();
  char *yyget_text();

  /* Required user-supplied function */
  #include <cstdio>
  void yyerror(const char *const s)
  {
      fprintf(stderr, "[YYERROR] %s, CONTEXT: %s, LINE: %d\n",
              s, yyget_text(), yyget_lineno());
  }
%}

%union
{
    float                f;
    int                  i;
    const char          *s;
    VRSValue            *value;
    void                *nothing;

    TArray<VRSValue *>  *csvalues;  /* Comma-separated values */
    TArray<int32>       *csints;
    TArray<FString>     *csstrings;
    TArray<FVector>     *csvectors;
}

%type   <f>             TOK_FLOAT
%type   <i>             TOK_INTEGER
%type   <s>             TOK_IDENTIFIER TOK_STRING
%type   <value>		value color matrix num transform vector
%type	<value>		list_generic list_int list_string list_vector
%type	<csvalues>	csvalues
%type	<csints>	csints
%type	<csstrings>	csstrings
%type	<csvectors>	csvectors
%type	<nothing>	object pair

%token TOK_ACOLOR
%token TOK_COLOR
%token TOK_FLOAT
%token TOK_IDENTIFIER
%token TOK_INTEGER
%token TOK_LIST
%token TOK_LIST_INT
%token TOK_LIST_STRING
%token TOK_LIST_VECTOR
%token TOK_MATRIX
%token TOK_STRING
%token TOK_TRANSFORM
%token TOK_VECTOR

%%

program:	program object
	|
	;

object:		TOK_IDENTIFIER TOK_IDENTIFIER '{' pair_list '}' {
                    VRSceneParser::GetActiveInstance()->NewObject($1, $2);
                    delete [] $1;
                    delete [] $2;
		    $$ = nullptr;
		}
	;

pair:		TOK_IDENTIFIER '=' value ';' {
		    VRSceneParser::GetActiveInstance()->NewPair($1, $3);
                    delete [] $1;
		    $$ = nullptr;
		}
	;

pair_list:	pair_list pair
        	|
        	;

value:          TOK_STRING     { $$ = VRSValue::NewString($1);    delete [] $1; }
	|	TOK_IDENTIFIER { $$ = VRSValue::NewReference($1); delete [] $1; }
	|	color          { $$ = $1; }
	|	matrix         { $$ = $1; }
	|	transform      { $$ = $1; }
        |       num            { $$ = $1; }
        |       vector         { $$ = $1; }
	|	list_generic   { $$ = $1; }
	|	list_int       { $$ = $1; }
	|	list_string    { $$ = $1; }
	|	list_vector    { $$ = $1; }
        ;

csvalues:	value {
		  $$ = new VRSValue::GenericListType();
		  $$->Reserve(1024);
		  $$->Add($1);
		}
	| 	csvalues ',' value {
	          $1->Add($3);
		  $$ = $1;
		}
	;

color:		TOK_COLOR  '(' num ',' num ',' num ')' {
                    $$ = VRSValue::NewColor(
			$3->GetFloat(), $5->GetFloat(), $7->GetFloat(), 1.0f
		    );
		    delete $3;
		    delete $5;
		    delete $7;
		}
	|	TOK_ACOLOR '(' num ',' num ',' num ',' num ')' {
		    $$ = VRSValue::NewColor(
			$3->GetFloat(), $5->GetFloat(), $7->GetFloat(), $9->GetFloat()
		    );
		    delete $3;
		    delete $5;
		    delete $7;
		    delete $9;
	        }
	;

matrix:		TOK_MATRIX '(' vector ',' vector ',' vector ')' {
		    $$ = VRSValue::NewMatrix($3->GetVector(), $5->GetVector(), -$7->GetVector());
		    delete $3;
		    delete $5;
		    delete $7;
		}
	;

num:            TOK_FLOAT      { $$ = VRSValue::NewFloat($1); }
        |       TOK_INTEGER    { $$ = VRSValue::NewInt($1); }
        ;

csints: 	TOK_INTEGER {
                    $$ = new VRSValue::IntListType();
		    $$->Reserve(1024);
		    $$->Add($1);
                }
	|	csints ',' TOK_INTEGER {
	            $1->Add($3);
	            $$ = $1;
		}

transform:	TOK_TRANSFORM '(' matrix ',' vector ')' {
		    $$ = VRSValue::NewTransform($3->GetMatrix(), $5->GetVector());
		    delete $3;
		    delete $5;
                }
	;

csstrings:	TOK_STRING {
		    $$ = new VRSValue::StringListType();
		    $$->Reserve(1024);
		    // TODO
		}
	|	csstrings ',' TOK_STRING {
	            $$ = $1;
		    // TODO
		}
	;

vector:		TOK_VECTOR '(' num ',' num ',' num ')' {
		    const float X =  $3->GetFloat();
		    const float Y =  $5->GetFloat();
		    const float Z = -$7->GetFloat();
                    $$ = VRSValue::NewVector(X, Y, Z);
                    delete $3;
                    delete $5;
                    delete $7;
                }
	;

csvectors:	vector {
		    $$ = new VRSValue::VectorListType();
		    $$->Reserve(1024);
		    $$->Add($1->GetVector());
		    delete $1;
		}
	|	csvectors ',' vector {
	            $1->Add($3->GetVector());
		    delete $3;
		    $$ = $1;
		}
	;

list_generic:	TOK_LIST '(' ')' { $$ = VRSValue::NewGenericList(); }
	|	TOK_LIST '(' csvalues ')' { $$ = VRSValue::NewGenericList($3); }
	;

list_int:	TOK_LIST_INT '(' ')' { $$ = VRSValue::NewIntList(); }
	|	TOK_LIST_INT '(' csints ')' { $$ = VRSValue::NewIntList($3); }
	;

list_string:	TOK_LIST_STRING '(' ')' { $$ = VRSValue::NewStringList(); }
	|	TOK_LIST_STRING '(' csstrings ')' { $$ = VRSValue::NewStringList($3); }
	;

list_vector:	TOK_LIST_VECTOR '(' ')' { $$ = VRSValue::NewVectorList(); }
	| 	TOK_LIST_VECTOR '(' csvectors ')' { $$ = VRSValue::NewVectorList($3); }
	;

%%
