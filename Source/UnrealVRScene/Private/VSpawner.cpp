// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealVRScene.h"
#include "VSpawner.h"

#include "VRSceneParser.h"
#include "VNode.h"

// Sets default values
AVSpawner::AVSpawner()
: AActor()
, VRSceneFilename()
, ScaleFactor(1.0f)
, BaseRotation(0.0f, 0.0f, 0.0f)
, Parser()
{
}

// Called when the game starts or when spawned
void AVSpawner::BeginPlay()
{
     Super::BeginPlay();
     InflateScene();
}

// Called every frame
void AVSpawner::Tick( float DeltaTime )
{
     Super::Tick( DeltaTime );
}

void AVSpawner::InflateScene()
{
     const bool Result = Parser.Parse(VRSceneFilename);
     for (auto& Obj : Parser.GetObjects())
     {
          if (Obj.Value->GetClassName() == TEXT("Node"))
          {
               SpawnNode(*Obj.Value);
          }
     }
}

void AVSpawner::SpawnNode(VRSObject& Node)
{
     UWorld *const World = GetWorld();
     if (World == nullptr)
     {
          return;
     }
     FActorSpawnParameters Params;
     Params.Owner = this;
     Params.Instigator = Instigator;
     UClass *const NodeClass = AVNode::StaticClass();
     AVNode *const Actor = World->SpawnActor<AVNode>(NodeClass, FVector::ZeroVector, BaseRotation, Params);
     Actor->LoadNodeFromVRSObject(Parser, Node, ScaleFactor);
}
