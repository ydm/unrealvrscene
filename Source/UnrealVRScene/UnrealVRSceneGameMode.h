// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "UnrealVRSceneGameMode.generated.h"

UCLASS(minimalapi)
class AUnrealVRSceneGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AUnrealVRSceneGameMode();
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Omege)
    TSubclassOf<class ACharacter> DefaultPawnClass2;
};
