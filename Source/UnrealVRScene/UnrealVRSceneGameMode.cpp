// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "UnrealVRScene.h"
#include "UnrealVRSceneGameMode.h"
#include "UnrealVRSceneHUD.h"

// #include "Private/VRSceneParser.h"


AUnrealVRSceneGameMode::AUnrealVRSceneGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/BP_VCharacter"));
    if (PlayerPawnClassFinder.Succeeded())
    {
        DefaultPawnClass = PlayerPawnClassFinder.Class;
    }

	// use our custom HUD class
	HUDClass = AUnrealVRSceneHUD::StaticClass();
}
